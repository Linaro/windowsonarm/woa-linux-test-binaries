# CMake generated Testfile for 
# Source directory: C:/work/osquery/tools/tests
# Build directory: C:/work/osquery/build/tools/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test([=[tools_tests_testosqueryd]=] "C:/wenv/arm64/cmake/bin/cmake.exe" "-E" "env" "PYTHONPATH=C:/work/osquery/build/python_path" "C:/Users/user/AppData/Local/Programs/Python/Python311-32/python.exe" "-u" "test_osqueryd.py" "--verbose" "--build" "C:/work/osquery/build" "--test-configs-dir" "C:/work/osquery/build/test_configs")
set_tests_properties([=[tools_tests_testosqueryd]=] PROPERTIES  TIMEOUT "300" WORKING_DIRECTORY "C:/work/osquery/build/tools/tests" _BACKTRACE_TRIPLES "C:/work/osquery/tools/tests/CMakeLists.txt;49;add_test;C:/work/osquery/tools/tests/CMakeLists.txt;92;addPythonTest;C:/work/osquery/tools/tests/CMakeLists.txt;12;generatePythonTests;C:/work/osquery/tools/tests/CMakeLists.txt;111;osqueryToolsTestsMain;C:/work/osquery/tools/tests/CMakeLists.txt;0;")
add_test([=[tools_tests_testosqueryi]=] "C:/wenv/arm64/cmake/bin/cmake.exe" "-E" "env" "PYTHONPATH=C:/work/osquery/build/python_path" "C:/Users/user/AppData/Local/Programs/Python/Python311-32/python.exe" "-u" "test_osqueryi.py" "--verbose" "--build" "C:/work/osquery/build" "--test-configs-dir" "C:/work/osquery/build/test_configs")
set_tests_properties([=[tools_tests_testosqueryi]=] PROPERTIES  TIMEOUT "300" WORKING_DIRECTORY "C:/work/osquery/build/tools/tests" _BACKTRACE_TRIPLES "C:/work/osquery/tools/tests/CMakeLists.txt;49;add_test;C:/work/osquery/tools/tests/CMakeLists.txt;93;addPythonTest;C:/work/osquery/tools/tests/CMakeLists.txt;12;generatePythonTests;C:/work/osquery/tools/tests/CMakeLists.txt;111;osqueryToolsTestsMain;C:/work/osquery/tools/tests/CMakeLists.txt;0;")
