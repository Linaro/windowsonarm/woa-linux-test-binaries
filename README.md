# woa-linux-bin

Binaries to evaluate woa-linux

Failures log (using wine-arm64) are kept in fails folder.

OSQuery
-------

Unittests from OSQuery. Failing tests (on Windows on Arm) were removed.

To run osquery for native windows (using wenv):

```
$ cmd.exe /c 'C:/wenv/arm64/activate.bat' bash -c './run_binaries.sh osquery'
```

To run osquery tests under wine-arm64 (using woa-linux):

```
$ /path/to/woa-linux/run.sh arm64 ./run_binaries.sh osquery
```

Numpy benchmarks
----------------

Python + numpy installed using pip (on Windows on Arm machine, to have access to
a working compiler cl.exe).

Numpy benchmarks from https://github.com/numpy/numpy/tree/main/benchmarks/benchmarks.

To run numpy benchmarks for native windows (using wenv):

```
$ cmd.exe /c 'C:/wenv/arm64/activate.bat' bash -c './run_numpy_bench.sh'
```

To run numpy benchmarks tests under wine-arm64 (using woa-linux):

```
$ /path/to/woa-linux/run.sh arm64 ./run_numpy_bench.sh
```

OpenSSL
-------

Unittests from OpenSSL
