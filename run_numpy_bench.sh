#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" >&2
    exit 1
}

script_dir=$(dirname $(readlink -f $0))

# if wine is one PATH, use it
prefix=$(which wine-arm64 || which wine || true)
python=$script_dir/python/python.exe
[ -f $python ] || die "missing $python interpreter"

for file in $script_dir/numpy-benchmarks/bench_*; do
    echo "---------------------------"
    echo $file
    $prefix $python $file "$@"
done
