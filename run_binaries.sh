#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" >&2
    exit 1
}

cd $(dirname $(readlink -f $0))

[ $# -ge 1 ] || die "usage: folder..."

# if wine is one PATH, use it
prefix=$(which wine-arm64 || which wine || true)

export LC_ALL=en_US.UTF-8

# osquery
export TEST_CONF_FILES_DIR=./osquery/test_configs/

mkdir -p output

for exe in $(find "$@" | grep exe$ | sort -u)
do
    echo "------------------------"
    echo $exe
    exit_code=0
    (timeout 60 $prefix $exe |& tee out.txt) || exit_code=$?
    echo "exit code: $exit_code" |& tee -a out.txt
    mkdir -p output/$(dirname $exe)
    mv out.txt output/$exe
    rm -f out.txt
done
